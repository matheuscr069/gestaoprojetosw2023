---

Autor: Thiago Serra Ferreira de Carvalho
E-mail: to.carvalho (at) univag.edu.br
Data: 03.08.2023
TOCTitle: Markdown

---

# Sobre o tipo de arquivo `md`

Os documentos `md` são arquivos textos com marcações especiais.
Normalmente são usados para escrita dos famosos `readme` que você encontra em repositórios de código.

A mágica dos arquivos `md` está na sua simplicidade de escrita. Você se preocupa com o _conteúdo_ e _**não** com a formatação_.

Um excelente guia para aprender as marcas está no próprio [GitLab](https://docs.gitlab.com/ee/user/markdown.html).
Outra fonte, em português, no [Github](https://docs.github.com/pt/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax).


**Não há segredo!**


A linguagem é _simples_ de aprender.


## Suporte nos editores
Se você utiliza o Visual Studio Code vai encontrar boas extensões para ajudá-lo na escrita dos documentos e pré-visualização do resultado. Algumas como o [Markdown Preview](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced) e 
o [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) fazem todo o trabalho para você.

Se você usa (n)Vim pode se interessar pelo plugin [markdownlint](https://github.com/igorshubovych/markdownlint-cli).

Fora isso, é possível usar editores específicos disponível gratuitamente como o [Dillinger](https://dillinger.io) e o [SlackEdit](https://slackedit.io).

