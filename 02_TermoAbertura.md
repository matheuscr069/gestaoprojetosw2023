---

Autor: Thiago Serra Ferreira de Carvalho
E-mail: thiago.carvalho (at) univag.edu.br
Data: 03.08.2023
TOCTitle: TermoAbertura

---

# Termo de abertura
<!--
    Autorizar o início do projeto, atribuir principais responsáveis e documentar 
    requisitos iniciais, principais entregas, premissas e restrições.
-->
Autorização: Jorge Washington, CEO. 
Principal responsável: Thiago Vargas, Gestor de Software

Requisitos Iniciais

-O projeto deve desenvolver uma opção de compartilhamento de viagens para o aplicativo de transporte.
-A opção deve permitir que os usuários encontrem outros usuários que estão viajando para o mesmo destino.
-A opção deve permitir que os usuários negociem o preço da viagem.
-A opção deve permitir que os usuários compartilhem informações sobre a viagem, como horários, locais de partida e chegada.

Principais Entregas:
-Uma versão beta da opção de compartilhamento de viagens deve ser lançada em seis meses.
-A versão final da opção deve ser lançada em um ano.

Premissas

-Os usuários estão dispostos a compartilhar suas viagens com outras pessoas.
-Os usuários estão dispostos a pagar uma taxa para compartilhar suas viagens.

Restrições

O projeto deve ser concluído dentro de um orçamento de R$ 100.000,00.
O projeto deve ser concluído dentro de um prazo de seis meses.

## Justificativa do projeto
<!--
    Em poucas palavras, um ou dois parágrafos, dizer o que levou a empresa a necessidade de implementar a solução.
--> 
- Solução Sustentável para a Mobilidade Urbana. 

As cidades estão enfrentando desafios crescentes relacionados ao congestionamento do tráfego e às emissões de poluentes. O compartilhamento de viagens apresenta uma alternativa sustentável, reduzindo o número de veículos nas estradas e contribuindo para uma mobilidade urbana mais eficiente e ecologicamente correta.

## Requisitos 
<!--
    O objetivo final das entrevistas é ouvir as partes interessadas e registrar suas necessidades.
    Os requisitos são registrados no seu maior nível de detalhe, pois são usados para definir os entregáveis ou marcos do projeto.
-->

- R1: A opção deve permitir que os usuários encontrem outros usuários que estão viajando para o mesmo destino.
    - A opção deve usar um algoritmo de correspondência para encontrar usuários que estão viajando para o mesmo destino.
    - O algoritmo deve levar em consideração fatores como a localização dos usuários, o horário da viagem e o número de   lugares disponíveis no veículo.
    - A opção deve fornecer uma lista de usuários que correspondem aos critérios de pesquisa.

- R2: A opção deve permitir que os usuários negociem o preço da viagem.
    - A opção deve fornecer uma interface para os usuários negociarem o preço da viagem.
    - A interface deve permitir que os usuários ofereçam e aceitem preços.
    - A opção deve registrar o preço final da viagem.

- R3:  A opção deve permitir que os usuários compartilhem informações sobre a viagem, como horários, locais de partida e chegada.
    - A opção deve fornecer uma interface para os usuários compartilharem informações sobre a viagem.
    - A interface deve permitir que os usuários informem o horário da viagem, o local de partida e o local de chegada.
    - A opção deve registrar essas informações.

## Premissas
<!-- 
    Aqui é o lugar para descrever o que é dado como certo para execução deste projeto.
    Por exemplo: se vamos desenvolver um software, pré supõe-se que a empresa tenha a infra estrutra para rodá-lo ou,
que possua o servidor de aplicação com requisito necessário para tal ou, ainda, que tenha pessoal técnico para ser treinado
a fim de dar suporte.
    Este é o local para fazer isso!
-->
- Os usuários estão dispostos a compartilhar suas viagens com outras pessoas.
    Isso significa que os usuários estão dispostos a abrir suas viagens para outras pessoas e permitir que elas viajem com eles. A opção deve ser promovida para os usuários e deve ser feita uma pesquisa para entender as necessidades dos usuários.

- Os usuários estão dispostos a pagar uma taxa para compartilhar suas viagens.
    Isso significa que os usuários estão dispostos a pagar uma taxa para compartilhar suas viagens com outras pessoas. A taxa deve ser competitiva e deve ser ajustada com base na demanda dos usuários.

Explicação das Premissas:
    A premissa de que os usuários estão dispostos a compartilhar suas viagens com outras pessoas é importante porque afeta o escopo e o sucesso do projeto. Se os usuários não estiverem dispostos a compartilhar suas viagens, o projeto não terá sucesso.

    A premissa de que os usuários estão dispostos a pagar uma taxa para compartilhar suas viagens é importante porque afeta o orçamento do projeto. Se os usuários não estiverem dispostos a pagar uma taxa, o projeto pode não ser viável.

## Restrições
<!--
    O que limita o funcionamento da solução que será desenvolvida ou implantada?
    O parque computacional da empresa?
    A falta de um processo definido?
    O tempo para implantação?
-->
Este projeto pode ser limitado devido a:
- Inadequação do parque tecnológico existente na empresa tendo em vista as máquinas apresentarem ....


# Aprovações
<!--
    Declaração de aceite é importantíssimo!
    Fechado o escopo e requisitos, o projeto terá um cronograma definido.
    Alterações no escopo ou requisitos, alteram o projeto, custo, tempo ...
    É sua única forma de defesa!
-->
    Este artefato, representando o projeto de desenvolvimento do Aplicativo de Transporte com Opção de Compartilhamento de Viagem, foi aprovado durante a reunião realizada em 15/11/2023. As seguintes pessoas estavam presentes durante a reunião de aprovação: 

Patrocinador: Jorge Washington
Gerente do Projeto: Thiago Vargas

    Durante a reunião, foram discutidos os objetivos, escopo, benefícios esperados e planos de implementação do projeto. Após revisão e consideração, o projeto foi aprovado para avançar para a próxima fase de execução.

Assinam este documento como prova de aprovação:

Assinatura do Patrocinador: Jorge Washington    Assinatura do Gerente de Projetos: Thiago Vargas
15/11/2023                                      15/11/2023

Esta aprovação formaliza o compromisso e apoio para a execução bem-sucedida do projeto, alinhado com os objetivos estratégicos da organização.

---
Ir para:
- [<< 01 - Plano do Projeto](01_PlanoProjeto.md)
- [03 - Cronograma >>](03_Cronograma.md)
