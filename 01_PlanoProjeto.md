---

Autor: Thiago Serra Ferreira de Carvalho
E-mail: thiago.carvalho (at) univag.edu.br
Data: 03.08.2023
TOCTitle: PlanoProjeto

---

# Plano do Projeto

## Grupo
<!--
    Isto é muito importante! A nota é do grupo, e todos precisam constar aqui, conforme exemplo,
nome completo, matrícula, curso e turma!
-->
- Luís Fernando Pinto, 2260000523, SIS 23/2
- Matheus Cruz Da Silva Campos, 3040303722, 23/2
- Rene Martins Schimoller, 2260000423, SIS 23/2
- Bruno Torres de Oliveira,3040002323, TAD 23/2
- Rodrigo Netto Ribeiro, 8500303522, SIS 23/2


## Nome do Projeto
<!--
    Seja criativo mas, principalmente, coerente com o objeto do projeto.
--->
Compartilhe a Viagem


# Plano de Entrevistas
<!-- 
    Aqui devem estar um plano simples, exemplo a seguir, das entrevistas que amparam o projeto.
    Lembre-se que os interessados no projeto são as pessoas que devem ser entrevistadas. Ao menos duas.
-->

## Cronograma de entrevistas

| Data   | Entrevistado | Função na empresa | Perguntas importantes      |
| :------: | :------------ | :----------------- | :-------------------------- |
| 10.01.23 | Julio Silva |profissional de pesquisa de mercado|  Você já pensou em compartilhar uma viagem com outras pessoas?|
| 12.01.23 | Roberto Silva | Gerente de Compras | Você estaria disposto a pagar uma taxa para compartilhar uma viagem? |


# Responsável pelo projeto
<!--
    Lembre-se as funções principais são:
    - Patrocinador: é a pessoa interessada pelo projeto, que tenha domínio do processo e seja capaz de definir ou decidir sobre o que será implementado.
    - Gerente do Projeto: é a pessoa que de fato gerenciará a equipe de projeto, no nosso exercício, apesar de haver um nome, todos no grupo exercem esse papel.
-->

O contratante estabelece as seguintes responsabilidades:
- Stakeholder principal: Jorge Washington, CEO. 
- Gerente do Projeto: Thiago Vargas, Gestor de Software


# Demanda inicial
<!--
    A partir do que ouviu nas entrevistas, qual é a demanda das áreas da empresa?
    O que elas eperam que o sistema entregue?
-->
- Facilidade na adesão ao sistema de compartilhamento de viagem.
- Soluções de transporte eficientes e econômicas para funcionários
- Interface simples e fácil de usar, suporte ao cliente eficiente e transparência nas operações do aplicativo.
- Algoritmos inteligentes para otimizar rotas, reduzindo o tempo de viagem e aumentando a eficiência operacional para motoristas e usuários.

# Escopo
<!--
    Uma explicação clara e direta sobre o que o projeto pretende atender/resolver. Pode usar até dois parágrafos.
    Lembre-se, aqui estará o objetivo do projeto.
-->
O escopo do projeto "Compartilhe a Viagem" é desenvolver uma opção de compartilhamento de viagens para o aplicativo de transporte. Essa opção permitirá que os usuários compartilhem suas viagens com outras pessoas, o que pode reduzir o custo das viagens e reduzir o congestionamento do trânsito.


---
Ir para:
- [02 - Termo de Abertura >>](02_TermoAbertura.md)


